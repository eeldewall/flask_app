var randomHexLetter = function() {
    return Math.floor(Math.random() * 256).toString(16);
}


var randomColor = function()  {
   return "#" + randomHexLetter() + randomHexLetter() + randomHexLetter();
}


document.onreadystatechange = function() {
    var element = document.getElementById('people');

    window.setInterval(function() {
        element.style.color = randomColor();
    }, 500);
}

