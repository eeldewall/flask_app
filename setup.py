#!/usr/bin/env python

from setuptools import setup

setup(
    name='Flask Test',
    install_requires=['Flask', 'Flask-Assets', 'pyscss']
)
