from flask import Flask, render_template
from flask.ext.assets import Environment, Bundle


app = Flask(__name__)
app.debug = True
assets = Environment(app)
assets.debug = True
assets.url = app.static_url_path
scss = Bundle(
    'scss/site.scss',
    'scss/colors.scss',
    filters='pyscss',
    output='stylesheet.css'
)

assets.register('scss_all', scss)


@app.route('/')
def index():
    people = [{'name': 'Erik', 'age': 18}, {'name': 'Johan', 'age': 21}]
    return render_template('index.html', people=people)


@app.route('/counter/<number>')
def counter(number):
    return render_template('counter.html', count=number)


@app.route('/site.js')
def javascript():
    return open('js/site.js').read()


if __name__ == '__main__':
    app.run(debug=True)
